import {useState, useEffect} from 'react';
import {Container, Card, Button} from 'react-bootstrap';


export default function SpecificProduct ({match}) {

	const [Name, setName] = useState('');
	const [Description, setDescription] = useState("");
	const [Price, setPrice] = useState(0);

	const [id, setId] = useState("")
	const [quantity, setQuantity] = useState(1);

	//set state to get the grand total of all subtotals
	const [total, setTotal] = useState(0);
	//set state for cart
	const [cart, setCart] = useState([]);
	//states to determine the current product the user can add to their cart



	// match.params holds the ID of our course in the courseId
	const productId = match.params.productId;
	console.log(productId);

	// Function to increment Button quantity input  by 1
	const quantityIncrement = () => {
	  setQuantity(quantity + 1);
	};

	// Function to decrement Button quantity input  by 1
	const quantityDecrement = () => {
	  	if(quantity > 1) {
			setQuantity(quantity - 1);
	  	}
	};



	// Bind the useState to useEffect
	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.Name);
			setDescription(data.Description);
			setPrice(data.Price);
			setId(productId);

		});
	}, [])

	//FOR GETTING YOUR LOCALSTORAGE CART ITEMS AND SETTING YOUR CART STATE:
	//on component mount, check if there is an existing cart in localStorage. If there is, set its contents as our cart state
	useEffect(()=> {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
		}
	}, []);

	//FOR UPDATING A STATE THAT DETERMINES THE TOTAL AMOUNT:
	//whenever our cart state changes, re-calculate the total
	useEffect(()=> {
		//start with a counter initialized to zero
		let tempTotal = 0

		//loop through our cart, getting each item's subtotal and incrementing our tempTotal counter by its amount
		cart.forEach((item)=> {
			tempTotal += item.subtotal
		})

		//set our total state
		setTotal(tempTotal)
	}, [cart])

	//FOR ADDING ITEMS TO CART:
	const addToCart = () => {
		/*
		//variable to determine if the item we are adding is already in our cart or not
		let alreadyInCart = false
		//variable for the item's index in the cart array, if it already exists there
		let productIndex
		//temporary cart array
		let cart = []

		if(localStorage.getItem('cart')){
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		//loop through our cart to check if the item we are adding is already in our cart or not
		for(let i = 0; i < cart.length; i++){
			if(cart[i].productId === id){
				//if it is, make alreadyInCart true
				alreadyInCart = true
				productIndex = i
			}
		}

		//if a product is already in our cart, just increment its quantity and adjust its subtotal
		if(alreadyInCart){
			cart[productIndex].quantity += quantity
			cart[productIndex].subtotal = cart[productIndex].Price * cart[productIndex].quantity
		}else{
			//else add a new entry in our cart, with values from states that need to be set wherever this function goes
			cart.push({
				'productId' : id,
				'Name': Name,
				'Price': Price,
				'quantity': quantity,
				'subtotal': Price * quantity
			})		
		}

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(cart))
		*/

		let alreadyInCart = false
				let productIndex
				let cart = []

				if(localStorage.getItem('cart')){
					cart = JSON.parse(localStorage.getItem('cart'))
				}

				for(let i = 0; i < cart.length; i++){
					if(cart[i].productId === id){
						alreadyInCart = true
						productIndex = i
					}
				}

				if(alreadyInCart){
					cart[productIndex].quantity = parseInt(cart[productIndex].quantity) + parseInt(quantity)
					cart[productIndex].subtotal = cart[productIndex].Price * cart[productIndex].quantity
				}

				else{
					cart.push({
						'productId' : id,
						'Name': Name,
						'Price': Price,
						'quantity': quantity,
						'subtotal': Price * quantity
					})		
				}

				localStorage.setItem('cart', JSON.stringify(cart))

				if(quantity === 1){
					alert("1 item added to cart.")
				}
				else{
					alert(`${quantity} items added to cart.`)
				}
	}

	//FOR ADJUSTING THE QUANTITY OF AN ITEM BASED ON INPUT:
	const quantityInput = (productId, value) => {

		//use the spread operator to create a temporary copy of our cart (from the cart state)
		let tempCart = [...cart]

		//loop through our tempCart 
		for(let i = 0; i < tempCart.length; i++){
			//so that we can find the item with the quantity we want to change via its productId
			if(tempCart[i].productId === productId){
				//use parseFloat to make sure our new quantity will be parsed as a number
				tempCart[i].quantity = parseFloat(value)
				//set the new subtotal
				tempCart[i].subtotal = tempCart[i].Price * tempCart[i].quantity
			}
		}

		//set our cart state with the new quantities
		setCart(tempCart)

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}

	

	// console.log(products);
	// console.log(cart);

	return (
		<Container className="mt-5">
			<Card>
				<Card.Header className="text-center"><h2>{Name}</h2></Card.Header>
				<Card.Body>

					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{Description}</Card.Text>

					<Card.Text>Price: <strong>&#8369;{Price}</strong></Card.Text>

					<Card.Text>Quantity:</Card.Text>
					<Card.Text>
					
					 	<Button variant="primary" size="sm" onClick={quantityDecrement}>-</Button>

					 	&nbsp;&nbsp;<input type="number" placeholder={quantity} />&nbsp;&nbsp;
					 	
					 	<Button variant="primary" size="sm" onClick={quantityIncrement}>+</Button>
					</Card.Text>

				</Card.Body>
				<Button variant="primary" onClick={addToCart}>Add to Cart</Button>
			</Card>

		</Container>
	);
}