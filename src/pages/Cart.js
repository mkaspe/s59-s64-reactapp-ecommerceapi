import {useState, useEffect, useContext} from 'react';
import {Table, Form, Button,} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Cart () {

	//set state to get the grand total of all subtotals
	const [total, setTotal] = useState(0)
	//set state for cart
	const [cart, setCart] = useState([])
	//states to determine the current product the user can add to their cart
	const [id, setId] = useState("")
	const [Name, setName] = useState("")
	const [Description, setDescription] = useState("")
	const [quantity, setQuantity] = useState(1);
	const [Price, setPrice] = useState(0)

	const [productsArr, setProductsArr] = useState([]);

	const token = localStorage.getItem("token")

	//FOR GETTING YOUR LOCALSTORAGE CART ITEMS AND SETTING YOUR CART STATE:
	//on component mount, check if there is an existing cart in localStorage. If there is, set its contents as our cart state
	useEffect(()=> {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
		}
	}, [])

	// Function to increment Button quantity input  by 1
	const quantityDecrement = (quantity, productId) => {
			quantity = parseInt(quantity);

			if(quantity > 1) {
				quantity --;
				quantityInput(productId, quantity)

			} else {
				alert("Quantity should be greater than 0");
			}
	}

	// Function to decrement Button quantity input  by 1
	const quantityIncrement = (quantity, productId) => {
		quantity = parseInt(quantity);
		quantity ++;
		quantityInput(productId, quantity);
	}

	useEffect(() => {

		const products = cart.map(product => {
			return (
				<>
					<tr key={product._id}>
					  <td>{product.Name}</td>
					  <td>&#8369; {product.Price}</td>
					  <td> 
					  		<Button variant="primary" type="submit" size="sm" onClick={() => quantityDecrement(product.quantity,product.productId)} >
					  			    -
					  		</Button>

					  		&nbsp;&nbsp; <input type="number" value={product.quantity}
					  		onChange={e => {
					  			if(e.target.value <= 0){
					  				alert("Quantity should be Lower than 0")
					  			} else {
					  				quantityInput(product.productId, e.target.value)
					  			}

					  		} }/>
					  		&nbsp;&nbsp;

					  		<Button variant="primary" type="submit" size="sm" onClick={() => quantityIncrement(product.quantity,product.productId)}>
					  		 	+
					  		</Button>
					
					  </td>
					  <td>{product.subtotal}</td>
					  <td><Button variant="danger" size="sm" onClick={removeItem}>Remove</Button></td>
					</tr>

				</>
			);
		})

		setProductsArr(products)

	})

	//FOR UPDATING A STATE THAT DETERMINES THE TOTAL AMOUNT:
	//whenever our cart state changes, re-calculate the total
	useEffect(()=> {
		//start with a counter initialized to zero
		let tempTotal = 0

		//loop through our cart, getting each item's subtotal and incrementing our tempTotal counter by its amount
		cart.forEach((item)=> {
			tempTotal += item.subtotal
		})

		//set our total state
		setTotal(tempTotal)
	}, [cart])

	//FOR ADJUSTING THE QUANTITY OF AN ITEM BASED ON INPUT:
	const quantityInput = (productId, value) => {

		//use the spread operator to create a temporary copy of our cart (from the cart state)
		let tempCart = [...cart]

		//loop through our tempCart 
		for(let i = 0; i < tempCart.length; i++){
			//so that we can find the item with the quantity we want to change via its productId
			if(tempCart[i].productId === productId){
				//use parseFloat to make sure our new quantity will be parsed as a number
				tempCart[i].quantity = parseFloat(value)
				//set the new subtotal
				tempCart[i].subtotal = tempCart[i].Price * tempCart[i].quantity
			}
		}

		//set our cart state with the new quantities
		setCart(tempCart)

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}

	//FOR REMOVING AN ITEM FROM CART:
	const removeItem = (productId) => {
		//use the spread operator to create a temporary copy of our cart (from the cart state)
		let tempCart = [...cart]

		//use splice to remove the item we want from our cart
		tempCart.splice([tempCart.indexOf(productId)], 1)

		//set our cart state with the new quantities
		setCart(tempCart)

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}
	
	function checkOut(){

			fetch(`${process.env.REACT_APP_API_URL}/orders/new-order`, {

				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					"products": cart,
					"totalAmount": total

				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
			})

			localStorage.removeItem('cart')

			alert("Order placed! thank you!");
	}

	return (
		<>
			<h1 className=" mt-5 mb-3 text-center">Your Shopping Cart</h1>
			<Table striped bordered hover className="">
			      <thead>
			        <tr>
			          <th>Name</th>
			          <th>Price</th>
			          <th>Quantity</th>
			          <th colSpan={2}>Subtotal</th>
			        </tr>
			      </thead>

			      <tbody>
			        {productsArr}
			        <tr>
			          <td colSpan={3}>

			          	<Link className="btn btn-success" to="/success-order" onClick={checkOut}>Checkout</Link> 
			          	
			          </td>

			          <th colSpan={2}> Total: {total}</th>
			        </tr>
			      </tbody>
			</Table>

		</>
	);
}

	