import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Products from '../pages/Products'

export default function Home () {

	const data = {
		title : "Welcome to MK DigiShop",
		content: "Your No. 1 Online Digital Store",
		destination: "/products",
		label: "View all product"
	}

	return (
		<>
			<Banner dataProp={data} />
			<Products />
		</>

	);
}