import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Logout () {

	// get the setUser Setter from App.js
	const { setUser } = useContext(UserContext);

	// clear localStorage
	localStorage.clear();

	// When the component mounts/page loads, set the user state to null
	// using the useEffect hook
	useEffect(() => {

			setUser({
				id: null,
				isAdmin: null
			});
	});


	return(

		// Redirect the user to login page or whenever Routes you want
		<Redirect to="/login" />
	);
}