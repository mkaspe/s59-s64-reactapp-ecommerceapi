import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner ({dataProp}) {

	const { title, content, destination, label } = dataProp;
	
	return( 
		<Row className="text-center mt-5">
			<Col className="mt-3 mb-3">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn btn-primary" to={destination}> {label} </Link>
			</Col>
		</Row>
	);
}