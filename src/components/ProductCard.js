import {useState, useEffect} from 'react';
import { Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard ({productProp}) {

	console.log(productProp);

	const { _id, Name, Description, Price } = productProp;

	return (

        <Row className="g-2 mt-3">
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>{Name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{Description}</Card.Text>

                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PhP {Price}</Card.Text>
 
                        <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
           
	);
}