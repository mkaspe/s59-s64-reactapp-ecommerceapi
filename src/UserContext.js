import React from 'react';


// Create a React Context Object
// A context object contains data that can be passed around to multiple props
// think of it like a delivery container or a box
const UserContext = React.createContext();

// A provider is what used to distribute the context object to the components
// think of it like a deliveryBOy or SantaClaus that deliver a package to the customer or a recipient
export const UserProvider = UserContext.Provider;


export default UserContext;